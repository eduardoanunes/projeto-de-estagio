package forttiori.controller;

import forttiori.entities.Endereco;
import forttiori.services.EnderecoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/projeto+estagio")
public class EnderecoController {

    @Autowired
    private EnderecoService enderecoService;

    @GetMapping("/busca+Endereco+Por/{cep}")
    @ApiOperation(value = "Retorna o endereço pelo cep")
    @ResponseStatus(HttpStatus.OK)
    public Endereco buscaEnderecoPor(@PathVariable String cep){
        return this.enderecoService.buscaEnderecoPor(cep);
    }
}
