package forttiori.controller;

import forttiori.entities.Product;
import forttiori.services.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/projeto+estagio")
@Api(value = "API REST Produtos")
@CrossOrigin(origins = "*")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/obter")
    @ApiOperation(value = "Retorna uma lista de produtos")
    @ResponseStatus(HttpStatus.OK)
    public List<Product> obterTodos(){ return this.productService.obterTodos();}

    @GetMapping("/obter+por+id/{id}")
    @ApiOperation(value = "Retorna uma produto pelo ID")
    @ResponseStatus(HttpStatus.OK)
    public Product obterPorId(@PathVariable String id){
        return this.productService.obterPorId(id);
    }

    @PostMapping("/criar")
    @ApiOperation(value = "Cria um produto")
    @ResponseStatus(HttpStatus.CREATED)
    public Product criar (@RequestBody @Valid Product product){return this.productService.criar(product);}

    @PutMapping("/alterar")
    @ApiOperation(value = "Altera um produto")
    @ResponseStatus(HttpStatus.OK)
    public Product alterar(@Valid Product product){
        return this.productService.alterar(product);
    }

    @PatchMapping("/alterarParcial/{price}")
    @ApiOperation(value = "Altera parcialmente um produto")
    @ResponseStatus(HttpStatus.OK)
    public Product alterarParcial(@Valid Product produto, @PathVariable double price){
        return this.productService.alterarParcial(produto, price);
    }

    @DeleteMapping("/deletar/{id}")
    @ApiOperation(value = "Exclui um produto pelo ID")
    @ResponseStatus(HttpStatus.OK)
    public void deletar(@PathVariable String id){
        this.productService.deletar(id);
    }



}
