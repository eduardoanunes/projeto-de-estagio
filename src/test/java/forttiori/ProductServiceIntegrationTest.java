package forttiori;

import forttiori.controller.ProductController;
import forttiori.entities.Product;
import forttiori.repository.ProductRepository;
import forttiori.services.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ProductController.class, ProductService.class})
class ProductServiceIntegrationTest {

    @MockBean
    ProductRepository productRepository;

    @Autowired
    ProductService productService;

    Product product = Product.builder()
            .id("1")
            .name("PS5")
            .brand("Sony")
            .type("Eletrônico")
            .price(4999.99)
            .amount(10)
            .build();

    @Test
    void deveObterPorId() {

        Mockito.when(productRepository.findById("1"))
                .thenReturn(java.util.Optional.ofNullable(product));

        var atual = productService.obterPorId(product.getId());
        var esperado = product;

        Assertions.assertEquals(esperado, atual);

    }
}