package forttiori;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoEstagioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoEstagioApplication.class, args);
	}

//	@Bean
//	public CommandLineRunner run(ViaCEPClient client){
//		return args -> {
//			if (args.length > 0) {
//				String cep = args[0];
//
//				Endereco endereco = client.buscaEnderecoPor(cep);
//
//				System.out.println(endereco);
//			}
//		};
//
//	}
}
