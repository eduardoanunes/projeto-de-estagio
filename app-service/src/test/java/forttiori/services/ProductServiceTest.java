package forttiori.services;

import forttiori.entities.Product;
import forttiori.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(SpringExtension.class)
class ProductServiceTest {

    @Mock
    ProductRepository productRepository;

    @InjectMocks
    ProductService productService;

    Product product = Product.builder()
            .id("1")
            .name("PS5")
            .brand("Sony")
            .type("Eletrônico")
            .price(4999.99)
            .amount(10)
            .build();

    List<Product> produtos = new ArrayList<>();

    @Test
    void deveRetornarSucessoObterPorId() {

        Mockito.when(productRepository.findById(product.getId()))
                .thenReturn(java.util.Optional.ofNullable(product));

        var atual = productService.obterPorId(product.getId());
        var esperado = product;

        assertEquals(esperado, atual);

    }
    @Test
    void deveRetornarProdutoNaoEncontrado() {
        Mockito.when(productRepository.findById(product.getId())).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "PRODUTO NÃO ENCONTRADO!"));
        var thrown = assertThrows(ResponseStatusException.class,
                () -> productRepository.findById("1"));
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
    }

    @Test
    void deveRetornarSucessoObterTodos() {

        Mockito.when(productRepository.findAll())
                .thenReturn(produtos);

        var atual = productService.obterTodos();
        var esperado = produtos;

        assertEquals(esperado, atual);

    }

}