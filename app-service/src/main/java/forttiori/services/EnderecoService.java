package forttiori.services;

import forttiori.entities.Endereco;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class EnderecoService {


    private RestTemplate restTemplateTimeout;

    public Endereco buscaEnderecoPor(String cep){
        try {
            return restTemplateTimeout.getForObject("/ws/{cep}/json", Endereco.class, cep);
        }catch (HttpClientErrorException ex){
            throw new RuntimeException("Client Error");
        }catch (Exception exception){
            throw new RuntimeException("Server Error");
        }
    }

}
