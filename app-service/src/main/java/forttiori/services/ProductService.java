package forttiori.services;

import forttiori.entities.Product;
import forttiori.exception.BadRequestException;
import forttiori.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository produtoRepository;

    public List<Product> obterTodos(){
        return this.produtoRepository.findAll();
    }

    public Product obterPorId(String id){
        return this.produtoRepository.findById(id).orElseThrow(() -> new BadRequestException("Produto não encontrado!"));
    }

    public Product criar(Product product){
        return this.produtoRepository.save(product);
    }

    public Product alterar(Product product){
        return this.produtoRepository.save(product);
    }

    public Product alterarParcial(Product produto, @PathVariable double price){
        produto.setPrice(price);
        return this.produtoRepository.save(produto);
    }

    public void deletar(String id){
        produtoRepository.deleteById(id);
    }


}
