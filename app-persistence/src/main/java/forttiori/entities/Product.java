package forttiori.entities;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document @Getter @Setter
@Builder @AllArgsConstructor
public class Product {
    @Id
    private String id;
    @Field("Name")
    private String name;
    @Field("Brand")
    private String brand;
    @Field("Type")
    private String type;
    @Field("Price")
    private double price;
    @Field("Amount")
    private int amount;

}
